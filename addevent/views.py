from django.shortcuts import render,get_object_or_404
from .models import RoomEvent
from django.db.models import Sum
from .forms import RoomEventForm
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse

# Create your views here.

def index(request):
	rec_rohith = RoomEvent.objects.filter(evnt_doer__contains='ohit').aggregate(Sum('evnt_doer_added_amount'))
	rec_jagadish = RoomEvent.objects.filter(evnt_doer__contains='agadish').aggregate(Sum('evnt_doer_added_amount'))
	rec_rishi = RoomEvent.objects.filter(evnt_doer__contains='ishi').aggregate(Sum('evnt_doer_added_amount'))
	rec_nikhil = RoomEvent.objects.filter(evnt_doer__contains='ikhil').aggregate(Sum('evnt_doer_added_amount'))
	rec_hemanth = RoomEvent.objects.filter(evnt_doer__contains='emant').aggregate(Sum('evnt_doer_added_amount'))
	rec_akshay = RoomEvent.objects.filter(evnt_doer__contains='ksha').aggregate(Sum('evnt_doer_added_amount'))
	rec_room = RoomEvent.objects.filter().aggregate(Sum('evnt_doer_added_amount'))
	rec_spent_room = RoomEvent.objects.filter().aggregate(Sum('evnt_amount'))
	context = {'rec_rohith':rec_rohith,'rec_jagadish':rec_jagadish,'rec_rishi':rec_rishi,'rec_nikhil':rec_nikhil,'rec_hemanth':rec_hemanth,'rec_akshay':rec_akshay,'rec_room':rec_room,'rec_spent_room':rec_spent_room,}
	return render(request, 'addevent/index.html',context)		 

def addevent(request):
	form = RoomEventForm(request.POST or None)
        if form.is_valid():
        	anevent = form.save(commit=False)
		anevent.save()
		return HttpResponseRedirect(reverse('addevent:ledger'))
	context= {"form":form,}
	return render(request,'addevent/addevent.html',context)

def ledger(request):
	allevents=RoomEvent.objects.all()
	context={'allevents':allevents,}
	return render(request,'addevent/ledger.html',context)
