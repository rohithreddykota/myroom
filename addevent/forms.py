from django import forms
from .models import RoomEvent

class RoomEventForm(forms.ModelForm):
	class Meta:
		model = RoomEvent
		fields=['evnt_doer','evnt_product','evnt_amount','evnt_doer_added_amount','evnt_details']

