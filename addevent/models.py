from __future__ import unicode_literals

from django.db import models #models is a package


# Create your models here.
class RoomEvent(models.Model):
	evnt_date=models.DateTimeField(auto_now=True)
	evnt_doer=models.CharField(max_length=255)
	evnt_product=models.CharField(max_length=255)
	evnt_amount=models.IntegerField(default=0)
	evnt_doer_added_amount=models.IntegerField(default=0)
	evnt_details=models.CharField(max_length=1023)

	def __str__(self):
		return str(self.evnt_date) + ' - ' + self.evnt_doer + ' - ' + self.evnt_product
