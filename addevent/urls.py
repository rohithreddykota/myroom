from django.conf.urls import url
from . import views

app_name = 'addevent'


urlpatterns = [
	url(r'^$',views.index,name='index'),
        url(r'^addevent/$',views.addevent,name='addevent'),
        url(r'^ledger/$',views.ledger,name='ledger'),
]
